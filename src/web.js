// React/Redux imports.
import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'

// Router imports.
import {
  routerReducer,
  ConnectedRouter,
  routerMiddleware
} from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

// Persist imports.
import {PersistGate} from 'redux-persist/lib/integration/react'

// Misc imports.
import {AppContainer} from 'react-hot-loader'
import BaseApp from './base'

// This is temporary and will be removed.
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

// When not in production add a debug command to show stuff
// in the console.
if (process.env.NODE_ENV !== 'production') {
  console.debug = (...args) => console.log(...args)
} else {
  console.debug = (...args) => {}
}

/**
 * The main application.
 */
export default class WebApp extends BaseApp {

  getReducers() {
    return {
      ...super.getReducers(),
      router: routerReducer
    }
  }

  getMiddleware() {
    this.history = createHistory()
    return [
      ...super.getMiddleware(),
      routerMiddleware(this.history)
    ]
  }

  getMainMount() {
    return document.getElementById('main-mount')
  }

  enableHmr() {
    if (module.hot) {
      const path = this.getMainComponentPath()
      module.hot.accept(path, () => {
        const NextMainView = this.loadMainComponent()
        this.render(NextMainView)
      })
    }
  }

  render(Component) {
    if (!Component) {
      Component = this.loadMainComponent()
    }
    render(
      <AppContainer>
        <Provider store={this.store}>
          <ConnectedRouter history={this.history}>
            <PersistGate
              persistor={this.persistor}
              loading={<div>Loading ...</div>}
            >
              <Component />
            </PersistGate>
          </ConnectedRouter>
        </Provider>
      </AppContainer>,
      this.getMainMount()
    )
  }

}
