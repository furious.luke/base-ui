// React Native
import {
  AppRegistry,
  View,
  Text
} from 'react-native'

// React/Redux imports.
import React from 'react'
import {Provider} from 'react-redux'

// Persist imports.
import {PersistGate} from 'redux-persist/lib/integration/react'

// Misc imports.
import BaseApp from './base'

/**
 * The main React Native application.
 */
export default class NativeApp extends BaseApp {

  getMainComponentPath() {
    return 'src/App.js'
  }

  render(Component) {
    if (!Component) {
      Component = this.loadMainComponent()
    }
    AppRegistry.registerComponent('ipsum', () => () => (
      <Provider store={this.store}>
        <PersistGate
          persistor={this.persistor}
          loading={<View><Text>Loading ...</Text></View>}
        >
          <Component />
        </PersistGate>
      </Provider>
    ))
  }

}
