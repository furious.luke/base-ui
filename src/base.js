// React/Redux imports.
import {
  createStore,
  applyMiddleware,
  compose,
  combineReducers
} from 'redux'

// Saga imports.
import createSagaMiddleware from 'redux-saga'

// Persist imports.
import {persistStore, persistReducer} from 'redux-persist'
import immutableTransform from 'redux-persist-transform-immutable'
import storage from 'redux-persist/es/storage'

// Misc imports.
import {responsiveStoreEnhancer, createResponsiveStateReducer} from 'redux-responsive'
import {enableBatching} from 'redux-batched-actions'

/**
 * The main application.
 */
export default class BaseApp {

  constructor() {
    this.rootReducer = null
    this.sagaMiddleware = null
    this.store = null
    this.persistor = null
  }

  getReducers() {
    return {
      browser: createResponsiveStateReducer(this.getResponsiveBreakpoints(), {infinity: 'large'})
    }
  }

  configureReducer(initialState = {}) {

    // Build our root reducer from 3rd party packages, and our
    // own internal reducers.
    let rootReducer = combineReducers(this.getReducers())

    // Include batching. Batching lets us send a few actions
    // through together to be processed simultaneously. Useful if we
    // need to do a few actions that cause heavy rerendering.
    rootReducer = enableBatching(rootReducer)

    // Add automatic local-storage persistance.
    rootReducer = persistReducer({
      key: 'root',
      whitelist: this.persistWhitelist,
      storage,
      transforms: [immutableTransform()]
    }, rootReducer)

    this.rootReducer = rootReducer
  }

  getResponsiveBreakpoints() {
    return {
      small: 560, // TODO: Probs smaller?
      medium: 700
    }
  }

  getMiddleware() {
    this.sagaMiddleware = createSagaMiddleware()
    return [
      this.sagaMiddleware
    ]
  }

  configureStore(initialState = {}) {

    // Compose enhancers and middleware.
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    const enhancer = composeEnhancers(
      responsiveStoreEnhancer,
      applyMiddleware(...this.getMiddleware())
    )
    const store = createStore(
      this.rootReducer,
      initialState,
      enhancer
    )
    const persistor = persistStore(store)
    this.store = store
    this.persistor = persistor
  }r

  getSagas() {
    return []
  }

  configureSaga() {
    function * rootSaga() {
      yield this.getSagas()
    }
    this.rootSaga = rootSaga.bind(this)
  }

  configure() {
    this.configureReducer()
    this.configureStore()
    this.configureSaga()
  }

  reducer() {
    return this.rootReducer
  }

  store() {
    return this.store
  }

  saga() {
    return this.rootSaga
  }

  csrfToken() {
    const state = this.store.getState()
    const csrfToken = (state.auth || {}).csrfToken
    return csrfToken
  }

  runSaga() {
    this.sagaMiddleware.run(this.saga())
  }

  run() {
    this.configure()
    this.runSaga()
    this.render()
  }

}
